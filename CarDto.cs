using System.Text.Json.Serialization;

public class CarDto {

        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [JsonPropertyName("make")]
        public string? Make { get; set; }

        [JsonPropertyName("model")]
        public string? Model { get; set; }

        [JsonPropertyName("year")]
        public int Year { get; set; }

        [JsonPropertyName("price")]
        public double Price { get; set; }

        [JsonPropertyName("drivable")]
        public bool Drivable { get; set; }

        [JsonPropertyName("registrationExpiry")]
        public DateTime? RegistrationExpiry { get; set; }

        [JsonPropertyName("mileage")]
        public int Mileage { get; set; }

        [JsonPropertyName("storeId")]
        public int StoreId { get; set; }
    }